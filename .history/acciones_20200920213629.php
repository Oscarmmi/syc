<?php

$BUSCAR_CLIENTES=1; 
$GUARDAR_CLIENTES=2;
$BUSCAR_ESTADOS_FACTURAS=3;
$GUARDAR_ESTADOS_FACTURAS=4;
$ELIMINAR_CLIENTE=5;

$accion=$_POST['accion'];

switch(intval($accion)){
    case $BUSCAR_CLIENTES:
        buscarClientes();
    break;
    case $GUARDAR_CLIENTES:
        guardarClientes();
    break;    
    case $BUSCAR_ESTADOS_FACTURAS:
        buscarEstadosFacturas();
    break;    
    case $GUARDAR_ESTADOS_FACTURAS:
        guardarEstadosFacturas();
    break;
    $ELIMINAR_CLIENTE
    case $GUARDAR_ESTADOS_FACTURAS:
        guardarEstadosFacturas();
    break;
}

function buscarEstadosFacturas(){
    $filtroDocumento="";
    if(isset($_POST['codi_estado'])){
        $filtroDocumento=" AND a.codi_estado=".$_POST['codi_estado'];
    }
    $sqlClientes="
        SELECT 
        a.*
        FROM estados_factura a   
        WHERE 
        1 
        $filtroDocumento       
    ";
    $resultado=consultar($sqlClientes);
    retornarArraydeDatos(array(
        'datos'=>$resultado
    ));
}

function buscarClientes(){
    $filtroDocumento="";
    if(isset($_POST['documento'])){
        $filtroDocumento=" AND a.nume_doc=".$_POST['documento'];
    }
    $sqlClientes="
        SELECT 
        a.*
        FROM clientes a   
        WHERE 
        1 
        $filtroDocumento       
    ";
    $resultado=consultar($sqlClientes);
    retornarArraydeDatos(array(
        'datos'=>$resultado
    ));
}

function validarEstadoFactura($datos){
    $respuesta=array();
    $respuesta['datos']=array();
    $respuesta['errores']=array();
    $aCliente=array();    
    if(!isset($datos['descripcion']) || $datos['descripcion']==''){
        $respuesta['errores'][]="- Debe ingresar una descripcion del estado";
    }else{
        $filtroCodigoAnterior="";  
        if(isset($datos['codi_estado'])){
            $filtroCodigoAnterior=" AND a.codi_estado!='$datos[codi_estado]' ";
        }
        $sqlDocumento="
            SELECT 
            a.* 
            FROM estados_factura a 
            WHERE 
            a.descripcion='$datos[descripcion]'  
            $filtroCodigoAnterior 
            LIMIT 1 
        ";
        $resultado=consultar($sqlDocumento);
        if(count($resultado)){
            $respuesta['errores'][]="- La descripcion del estado debe ser unica, la ingresada ya existe en la base de datos";
        }else{
            $respuesta['datos']['descripcion']=$datos['descripcion'];
        }   
    }        
    return $respuesta;
}

function validarDatosCliente($datos){
    $respuesta=array();
    $respuesta['datos']=array();
    $respuesta['errores']=array();
    $aCliente=array();    
    if(!isset($datos['nombre']) || $datos['nombre']==''){
        $respuesta['errores'][]="- Debe ingresar el nombre del Cliente";
    }else{
        $respuesta['datos']['nombre']=$datos['nombre'];
    }
    if(!isset($datos['direccion']) || $datos['direccion']==''){
        $respuesta['errores'][]="- Debe ingresar la dirección del Cliente";
    }else{
        $respuesta['datos']['direccion']=$datos['direccion'];
    }
    if(!isset($datos['documento']) || $datos['documento']==''){
        $respuesta['errores'][]="- Debe ingresar el numero de documento del Cliente";
    }else{      
        $filtroDocumentoAnterior="";  
        if(isset($datos['documentoanterior'])){
            $filtroDocumentoAnterior=" AND a.nume_doc!='$datos[documentoanterior]' ";
        }
        $sqlDocumento="
            SELECT 
            a.* 
            FROM clientes a 
            WHERE 
            a.nume_doc='$datos[documento]' 
            $filtroDocumentoAnterior 
            LIMIT 1 
        ";
        $resultado=consultar($sqlDocumento);
        if(count($resultado)){
            $respuesta['errores'][]="- El numero de documento del Cliente debe ser unico, el ingresado ya existe en la base de datos";
        }else{
            $respuesta['datos']['documento']=$datos['documento'];
        }        
    }    
    return $respuesta;
}

function guardarEstadosFacturas(){
    $datos=json_decode($_POST['datos'], true);    
    $aRespuesta=validarEstadoFactura($datos);    
    if(count($aRespuesta['errores'])){
        retornarArraydeDatos(array(
            'errores'=>$aRespuesta['errores']
        ));
        return;
    }
    $aDatos=$aRespuesta['datos'];
    $guardarEstadoFactura="INSERT INTO estados_factura(descripcion) VALUES('$aDatos[descripcion]');";
    if(isset($datos['codigo_estado'])){
        $guardarEstadoFactura=" UPDATE estados_factura SET descripcion=$aDatos[descripcion] WHERE codi_estado=$datos[codi_estado];";
    }
    ejecutarSentencia($guardarEstadoFactura);
    buscarEstadosFacturas();
}

function guardarClientes(){
    $datos=json_decode($_POST['datos'], true);    
    $aRespuesta=validarDatosCliente($datos);    
    if(count($aRespuesta['errores'])){
        retornarArraydeDatos(array(
            'errores'=>$aRespuesta['errores']
        ));
        return;
    }
    $aDatos=$aRespuesta['datos'];
    $guardarCliente="INSERT INTO clientes(nume_doc, nombre, direccion) VALUES($aDatos[documento], '$aDatos[nombre]', '$aDatos[direccion]');";
    if(isset($datos['documentoanterior'])){
        $guardarCliente=" UPDATE clientes SET nume_doc=$aDatos[documento], nombre='$aDatos[nombre]', direccion='$aDatos[direccion]' WHERE nume_doc=$datos[documentoanterior];";
    }
    ejecutarSentencia($guardarCliente);
    buscarClientes();
}

function ejecutarSentencia($sql){
    $conexion=conexion();
    mysqli_query( $conexion, $sql ) or die ( "Algo ha ido mal en la consulta a la base de datos");    
}

function retornarArraydeDatos($aDatos){
    echo json_encode($aDatos, true);
}

function consultar($sql){
    $conexion=conexion();
    $resultado = mysqli_query( $conexion, $sql ) or die ( "Algo ha ido mal en la consulta a la base de datos");
    $datosF=array();
    $campos=array();
    $camposT=array();
    while ($registros = mysqli_fetch_array( $resultado )){
        if(!count($campos)){
            $camposT=array_keys($registros);    
            foreach($camposT as $campo){
                if(!is_numeric($campo)){
                    $campos[]=$campo;
                }                
            }
        }
        $datosR=array();
        foreach($campos as $campo){
            $datosR[$campo]=$registros[$campo];
        }
        $datosF[]=$datosR;        
    }
    return $datosF;
}

function conexion(){
    $usuario = "root";
    $contrasena = "";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
    $servidor = "localhost";
    $basededatos = "syc";
    $conexion = mysqli_connect( $servidor, $usuario, "" ) or die ("No se ha podido conectar al servidor de Base de datos");
    $db = mysqli_select_db( $conexion, $basededatos ) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );
    return $conexion;
}

?>