
let rutaPpal='acciones.php';
let indicadorTablaClientes=0;

let BUSCAR_CLIENTES=1;
let GUARDAR_CLIENTES=2;

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){
        buscarClientes();
    });
    $("#btnMostrarModalCliente").click(function(){
        abrirModalClientes();
        $(".datoscliente").val('');
        $("#modalClientes").modal('show');
    });    
    $("#btnGuardarCliente").click(validarDatosCliente);
}

function abrirModalClientes(){

}

function validarDatosCliente(){
    let datos={};    
    if($("#clientes_documentoanterior").val()!='' && !isNaN($("#clientes_documentoanterior").val())){
        datos.documentoanterior=$("#clientes_documentoanterior").val();
    }    
    if($("#clientes_nombres").val()==''){
        alertify.error("- Debe ingresar el nombre del Cliente");
        return;
    }
    datos.nombre=$("#clientes_nombres").val();        
    if($("#clientes_direccion").val()==''){
        alertify.error("- Debe ingresar la dirección del Cliente");
        return;
    }
    datos.direccion=$("#clientes_direccion").val(); 
    if($("#clientes_numerodocumento").val()==''){
        alertify.error("- Debe ingresar el numero de documento del Cliente");
        return;
    }
    if(isNaN($("#clientes_numerodocumento").val())){
        alertify.error("- El numero de documento debe ser un valor numerico");
        return;
    }
    datos.documento=$("#clientes_numerodocumento").val();
    guardarCliente(datos);
}

function guardarCliente(datos){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: GUARDAR_CLIENTES, 
            datos:JSON.stringify(datos)
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            if(respuesta.errores){
                for (let a in respuesta.errores) {
                    alertify.error(respuesta.errores[a]);    
                }
                return;
            }
            alertify.success("- El cliente fue agregado exitosamente");
            $("#modalClientes").modal('hide');
            cargarTablaClientes(respuesta.datos);
        }
    });   
}

function buscarClientes(){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: BUSCAR_CLIENTES
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            cargarTablaClientes(respuesta.datos);
        }
    });    
}

function cargarTablaCorrespondiente(idtabla, datos) {
    var tabla = $(idtabla).dataTable();
    tabla.fnClearTable();
    if (datos.length > 0) {
        tabla.fnAddData(datos);
    }
}

function cargarTablaClientes(datos){
    if (!parseInt(datos.length)) {
        datos = [];
    }
    if(indicadorTablaClientes){
        cargarTablaCorrespondiente('#tabla_clientes', datos);
        return;
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'nombre',
                title: 'Nombre del Cliente',
                className: 'text-capitalize'
            },
            {
                data: 'direccion',
                title: 'Dirección',
                className: 'text-capitalize'
            },
            {
                data: 'nume_doc',
                title: 'Número de Documento',
                className: 'text-capitalize dt-body-center'
            },
            {
                data: 'nume_doc',
                title: 'Editar',
                className: 'text-capitalize dt-body-center',
                render: function (data, type, full, meta) {
                    return '<a style="cursor:pointer;" click=""><i class="fa fa-pencil"></i></a>';
                }
            }
        ]
    });
    indicadorTablaClientes++;
}