

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){

    });
}

function buscarClientes(){
    if (parseInt(datos.length) === 0) {
        datos = [];
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'descripcion',
                title: 'Descripcion de la Pregunta',
                className: 'text-capitalize'
            },
            {
                data: 'observacion',
                title: 'Observacion',
                className: 'text-capitalize'
            },
            {
                data: 'idpreguntarespuesta',
                title: 'Respondida',
                className: 'text-capitalize dt-body-center',
                render: function (data, type, full, meta) {
                    let html = 'No';
                    if (data) {
                        html = 'Si';
                    }
                    return html;
                }
            }
        ],
        fnRowCallback: function (nRow, aData) {
            if (aData.idpreguntarespuesta) {
                $(nRow).prop('title', 'Pregunta resuelta');
                $(nRow).css({
                    "color": "#5cb85c"
                });
            } else {
                $(nRow).prop('title', 'Sin responder');
                $(nRow).css({
                    "color": "#337ab7"
                });
            }
            return nRow;
        }
    });
    $('#' + id + ' tbody').on('click', 'tr', function () {
        abrirModalRespuestas(tabla.row(this).data());
    });
}

function crearTablaCorrespondiente(id, datos) {
    
}