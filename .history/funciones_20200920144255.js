
let rutaPpal='acciones.php';

let BUSCAR_CLIENTES=1;
let GUARDAR_CLIENTES=2;

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){
        buscarClientes();
    });
    $("#btnMostrarModalCliente").click(function(){
        $(".datoscliente").val('');
        $("#modalClientes").modal('show');
    });    
    $("#btnGuardarCliente").click(validarDatosCliente);
}

function validarDatosCliente(){
    let datos={};
    datos.documentoanterior=0;
    if($("#clientes_documentoanterior").val()!='' && !isNaN($("#clientes_documentoanterior").val())){
        datos.documentoanterior=$("#clientes_documentoanterior").val();
    }    
    if($("#clientes_nombres").val()==''){
        alertify.error("- Debe ingresar el nombre del Cliente");
        return;
    }
    datos.nombre=$("#clientes_nombres").val();        
    if($("#clientes_direccion").val()==''){
        alertify.error("- Debe ingresar la dirección del Cliente");
        return;
    }
    datos.direccion=$("#clientes_direccion").val(); 
    if($("#clientes_numerodocumento").val()==''){
        alertify.error("- Debe ingresar el numero de documento del Cliente");
        return;
    }
    if(isNaN($("#clientes_numerodocumento").val())){
        alertify.error("- El numero de documento debe ser un valor numerico");
        return;
    }
    datos.documento=$("#clientes_numerodocumento").val();
    guardarCliente(datos);
}

function guardarCliente(datos){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: GUARDAR_CLIENTES, 
            datos:JSON.stringify(datos)
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            alertify.success("- El numero de documento debe ser un valor numerico");
            cargarTablaClientes(respuesta.datos);
        }
    });   
}

function buscarClientes(){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: BUSCAR_CLIENTES
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            cargarTablaClientes(respuesta.datos);
        }
    });    
}

function cargarTablaClientes(datos){
    if (!parseInt(datos.length)) {
        datos = [];
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'nombre',
                title: 'Nombre del Cliente',
                className: 'text-capitalize'
            },
            {
                data: 'direccion',
                title: 'Dirección',
                className: 'text-capitalize'
            },
            {
                data: 'nume_doc',
                title: 'Número de Documento',
                className: 'text-capitalize dt-body-center'
            }
        ]
    });
    $('#tabla_clientes tbody').on('click', 'tr', function () {
        
    });
}

function crearTablaCorrespondiente(id, datos) {
    
}