

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){

    });
}

function buscarClientes(){
    $.ajax({
    url: 'acciones.php',
    data: {
        accion: BUSCAR_CLIENTES,
        idtipodedocumento: idtipodedocumento,
        numerodocumento: numerodocumento
    },
    type: 'POST',
    dataType: 'json',
    success: function (respuesta) {
        $("#ingresarDatosUsuarios").removeClass('hidden');
        $("#graficas").removeClass('hidden');
        limpiarCamposUsuario();
        if (!respuesta.mensajeerror) {
            cargarDatosenCamposCorrespondientes("#usuario_", respuesta.datos);
            crearPestannasxCategorias(respuesta.categorias);
            crearTablasCorrespondientesalasCategorias(respuesta.categorias, respuesta.preguntasxcategoria);
            alertify.success("- El usuario ya se encuentra registrado");
        } else {
            $("#usuario_idtipodedocumento").val(idtipodedocumento);
            $("#usuario_numerodocumento").val(numerodocumento);
            alertify.error(respuesta.mensajeerror);
        }
    }
    if (parseInt(datos.length) === 0) {
        datos = [];
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'nombre',
                title: 'Nombre del Cliente',
                className: 'text-capitalize'
            },
            {
                data: 'direccion',
                title: 'Dirección',
                className: 'text-capitalize'
            },
            {
                data: 'nume_doc',
                title: 'Número de Documento',
                className: 'text-capitalize dt-body-center'
            }
        ]
    });
    $('#tabla_clientes tbody').on('click', 'tr', function () {
        
    });
}

function crearTablaCorrespondiente(id, datos) {
    
}