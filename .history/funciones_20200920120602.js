let BUSCAR_CLIENTES=1;

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){
        buscarClientes();
    });
    $("#btnMostrarModalCliente").click(function(){
        $(".datoscliente").val('');
        $("#modalClientes").modal('show');
    });    
    $("#btnGuardarCliente").click(validarDatosCliente);
}

function validarDatosCliente(){
    let datos={};
    let validado=0;
    $("#clientes_idcliente").val();
    datos.nombre=$("#clientes_nombres").val();
    if(datos.nombre){

    }
    datos.direccion=$("#clientes_nombres").val();
    datos.$("#clientes_numerodocumento").val();
    if(validado){
        guardarCliente(datos);
    }
}

function buscarClientes(){
    $.ajax({
        url: 'acciones.php',
        data: {
            accion: BUSCAR_CLIENTES
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            cargarTablaClientes(respuesta.datos);
        }
    });    
}

function cargarTablaClientes(datos){
    if (!parseInt(datos.length)) {
        datos = [];
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'nombre',
                title: 'Nombre del Cliente',
                className: 'text-capitalize'
            },
            {
                data: 'direccion',
                title: 'Dirección',
                className: 'text-capitalize'
            },
            {
                data: 'nume_doc',
                title: 'Número de Documento',
                className: 'text-capitalize dt-body-center'
            }
        ]
    });
    $('#tabla_clientes tbody').on('click', 'tr', function () {
        
    });
}

function crearTablaCorrespondiente(id, datos) {
    
}