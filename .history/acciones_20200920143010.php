<?php

$BUSCAR_CLIENTES=1; 
$GUARDAR_CLIENTES=2;

$accion=$_POST['accion'];

switch(intval($accion)){
    case $BUSCAR_CLIENTES:
        buscarClientes();
    break;
    case $GUARDAR_CLIENTES:
        guardarClientes();
    break;
}

function buscarClientes(){
    $sqlClientes="
        SELECT 
        a.*
        FROM clientes a         
    ";
    $resultado=consultar($sqlClientes);
    retornarArraydeDatos(array(
        'datos'=>$resultado
    ));
}

function validarDatosCliente($datos){
    $respuesta=array();
    $respuesta['validado']=0;
    $respuesta['datos']=array();
    $respuesta['errores']=array();
    $aCliente=null;
    if($datos['creacion']==0){
        $sqlDocumento="
            SELECT 
            a.* 
            FROM clientes a 
            WHERE 
            a.nume_doc='$datos[documento]' 
            LIMIT 1 
        ";
        $resultado=consultar($sqlDocumento);
    }
    if(!isset($datos['nombre']) || $datos['nombre']==''){
        $respuesta['errores'][]="- Debe ingresar el nombre del Cliente";
    }else{
        $respuesta['datos']['nombre']=$datos['nombre'];
    }
    if(!isset($datos['direccion']) || $datos['direccion']==''){
        $respuesta['errores'][]="- Debe ingresar la dirección del Cliente";
    }else{
        $respuesta['datos']['direccion']=$datos['direccion'];
    }
    if(!isset($datos['documento']) || $datos['documento']==''){
        $respuesta['errores'][]="- Debe ingresar el numero de documento del Cliente";
    }else{
        $sqlDocumento="
            SELECT 
            a.* 
            FROM clientes a 
            WHERE 
            a.nume_doc='$datos[documento]' 
            LIMIT 1 
        ";
        $resultado=consultar($sqlDocumento);
        if(count($resultado)){
            $respuesta['errores'][]="- El numero de documento del Cliente debe ser unico, el ingresado ya existe en la base de datos";
        }else{
            $respuesta['datos']['documento']=$datos['documento'];
        }        
    }    
    return $respuesta;
}

function guardarClientes(){
    $datos=json_decode($_POST['datos'], true);
    var_dump($datos);exit;
    validarDatosCliente($datos);    
}

function retornarArraydeDatos($aDatos){
    echo json_encode($aDatos, true);
}

function consultar($sql){
    $conexion=conexion();
    $resultado = mysqli_query( $conexion, $sql ) or die ( "Algo ha ido mal en la consulta a la base de datos");
    $datosF=array();
    $campos=array();
    $camposT=array();
    while ($registros = mysqli_fetch_array( $resultado )){
        if(!count($campos)){
            $camposT=array_keys($registros);    
            foreach($camposT as $campo){
                if(!is_numeric($campo)){
                    $campos[]=$campo;
                }                
            }
        }
        $datosR=array();
        foreach($campos as $campo){
            $datosR[$campo]=$registros[$campo];
        }
        $datosF[]=$datosR;        
    }
    return $datosF;
}

function conexion(){
    $usuario = "root";
    $contrasena = "";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
    $servidor = "localhost";
    $basededatos = "syc";
    $conexion = mysqli_connect( $servidor, $usuario, "" ) or die ("No se ha podido conectar al servidor de Base de datos");
    $db = mysqli_select_db( $conexion, $basededatos ) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );
    return $conexion;
}

?>