
let rutaPpal='acciones.php';
let indicadorTablaClientes=0;
let indicadorTablaEstadosFacturas=0;

let BUSCAR_CLIENTES=1;
let GUARDAR_CLIENTES=2;
let BUSCAR_ESTADOS_FACTURAS=3;
let GUARDAR_ESTADOS_FACTURAS=4;

$(document).ready(function () {
    eventosClick();
    $("#liclientes").click();    
});

function eventosClick(){
    $("#liclientes").click(function(){
        buscarClientes();
    });
    $("#liestadosfacturas").click(function(){
        buscarEstadosFacturas();
    });
    $("#btnMostrarModalCliente").click(function(){
        abrirModalClientes();        
    });  
    $("#btnMostrarModalEstadosClientes").click(function(){
        abrirModalEstadosFacturas();        
    });
    $("#btnGuardarCliente").click(validarDatosCliente);
    $("#btnGuardarEstadoFactura").click(validarDatosEstadosFactura);
}

function buscarEstadosFacturas(codi_estado=undefined){
    let datos={
        accion: BUSCAR_ESTADOS_FACTURAS
    };
    if(codi_estado){
        datos.codi_estado=codi_estado;
    }
    $.ajax({
        url: rutaPpal,
        data: datos, 
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            if(codi_estado){
                cargarDatosEstadosFacturas(respuesta.datos[0]);
                return;
            }
            cargarTablaEstadosFacturas(respuesta.datos);
        }
    });  
}

function abrirModalEstadosFacturas(codi_estado){
    $(".estadosfacturas").val('');
    $("#modalEstadosFacturas").modal('show');
    if(codi_estado){
        buscarEstadosFacturas(codi_estado);
    }
}

function abrirModalClientes(documento=undefined){
    $(".datoscliente").val('');
    $("#modalClientes").modal('show');
    if(documento){
        buscarClientes(documento);
    }
}

function validarDatosEstadosFactura(){
    let datos={};    
    if($("#estadosfacturas_codi_estado").val()!='' && !isNaN($("#estadosfacturas_codi_estado").val())){
        datos.codi_estado=$("#estadosfacturas_codi_estador").val();
    }       
    if($("#estadosfacturas_descripcion").val()==''){
        alertify.error("- Debe ingresar la descripcion del estado");
        return;
    }
    datos.descripcion=$("#estadosfacturas_descripcion").val();
    guardarEstadoFactura(datos);
}

function validarDatosCliente(){
    let datos={};    
    if($("#clientes_documentoanterior").val()!='' && !isNaN($("#clientes_documentoanterior").val())){
        datos.documentoanterior=$("#clientes_documentoanterior").val();
    }    
    if($("#clientes_nombres").val()==''){
        alertify.error("- Debe ingresar el nombre del Cliente");
        return;
    }
    datos.nombre=$("#clientes_nombres").val();        
    if($("#clientes_direccion").val()==''){
        alertify.error("- Debe ingresar la dirección del Cliente");
        return;
    }
    datos.direccion=$("#clientes_direccion").val(); 
    if($("#clientes_numerodocumento").val()==''){
        alertify.error("- Debe ingresar el numero de documento del Cliente");
        return;
    }
    if(isNaN($("#clientes_numerodocumento").val())){
        alertify.error("- El numero de documento debe ser un valor numerico");
        return;
    }
    datos.documento=$("#clientes_numerodocumento").val();
    guardarCliente(datos);
}

function guardarEstadoFactura(datos){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: GUARDAR_ESTADOS_FACTURAS, 
            datos:JSON.stringify(datos)
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            if(respuesta.errores){
                for (let a in respuesta.errores) {
                    alertify.error(respuesta.errores[a]);    
                }
                return;
            }
            alertify.success("- El estado de factura fue agregado exitosamente");
            $("#modalEstadosFacturas").modal('hide');
            cargarTablaEstadosFacturas(respuesta.datos);
        }
    });   
}

function guardarCliente(datos){
    $.ajax({
        url: rutaPpal,
        data: {
            accion: GUARDAR_CLIENTES, 
            datos:JSON.stringify(datos)
        },
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            if(respuesta.errores){
                for (let a in respuesta.errores) {
                    alertify.error(respuesta.errores[a]);    
                }
                return;
            }
            alertify.success("- El cliente fue agregado exitosamente");
            $("#modalClientes").modal('hide');
            cargarTablaClientes(respuesta.datos);
        }
    });   
}

function buscarClientes(documento=undefined){
    let datos={
        accion: BUSCAR_CLIENTES
    };
    if(documento){
        datos.documento=documento;
    }
    $.ajax({
        url: rutaPpal,
        data: datos, 
        type: 'POST',
        dataType: 'json',
        success: function (respuesta) {
            if(documento){
                cargarDatosCliente(respuesta.datos[0]);
                return;
            }
            cargarTablaClientes(respuesta.datos);
        }
    });    
}

function cargarDatosEstadosFacturas(datos){
    $("#estadosfacturas_codi_estado").val(datos.codi_estado);
    $("#estadosfacturas_descripcion").val(datos.descripcion);
}

function cargarDatosCliente(datos){
    $("#clientes_documentoanterior").val(datos.nume_doc);
    $("#clientes_nombres").val(datos.nombre);
    $("#clientes_numerodocumento").val(datos.nume_doc);
    $("#clientes_direccion").val(datos.direccion);
}

function cargarTablaCorrespondiente(idtabla, datos) {
    var tabla = $(idtabla).dataTable();
    tabla.fnClearTable();
    if (datos.length > 0) {
        tabla.fnAddData(datos);
    }
}

function cargarTablaClientes(datos){
    if (!parseInt(datos.length)) {
        datos = [];
    }
    let tabla = $('#tabla_clientes').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'nombre',
                title: 'Nombre del Cliente',
                className: 'text-capitalize'
            },
            {
                data: 'direccion',
                title: 'Dirección',
                className: 'text-capitalize'
            },
            {
                data: 'nume_doc',
                title: 'Número de Documento',
                className: 'text-capitalize dt-body-center'
            },
            {
                data: 'nume_doc',
                title: 'Editar',
                className: 'text-capitalize dt-body-center',
                render: function (data, type, full, meta) {
                    return '<a style="cursor:pointer;" onclick="abrirModalClientes('+data+');" title="Editar este Registro"><i class="fa fa-pencil"></i></a>';
                }
            }
        ]
    });
    indicadorTablaClientes++;
}

function cargarTablaEstadosFacturas(datos){
    if (!parseInt(datos.length)) {
        datos = [];
    }
    let tabla = $('#tabla_estadosfacturas').DataTable({
        paging: 'numbers',
        bFilter: false,
        destroy: true,
        select: true,
        dom: 'T<"clear">lfrtip', // Permite cargar la herramienta tableTools
        tableTools: {
            aButtons: [],
            sRowSelect: "single"
        },
        data: datos,
        columns: [{
                data: 'descripcion',
                title: 'Descripción del Estado',
                className: 'text-capitalize'
            },
            {
                data: 'codi_estado',
                title: 'Código del Estado',
                className: 'text-capitalize dt-body-center'
            },
            {
                data: 'codi_estado',
                title: 'Editar',
                className: 'text-capitalize dt-body-center',
                render: function (data, type, full, meta) {
                    return '<a style="cursor:pointer;" onclick="abrirModalEstadosFacturas('+data+');" title="Editar este Registro"><i class="fa fa-pencil"></i></a>';
                }
            }
        ]
    });
    indicadorTablaEstadosFacturas++;
}